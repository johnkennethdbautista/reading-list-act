// Post Database
// 2.
db.posts.insertMany([
	{
		title: "Hello World!", 
		content: "This is my first blog!", 
		likes: 5
	}, 
	{
		title: "Hello Guys!",
		content: "Hello everyone, welcome to my blog!",
		likes: 10
	},
	{
		title: "My first posts",
		content: "What's up everyone",
		likes: 0
	} 
]);

// 3.
db.posts.find();

// 4.
db.posts.insertOne({
	title: "Test",
	content: "This is only a tests posts"
});

// 5.
db.posts.find();

// 6.
db.posts.updateOne(
	{title: "Test"},
	{
		$set: {
			title: "This is my second posts",
			content: "Hello friends. Let's chat!",
			likes: 0
		}
	}
);

// 7. 
db.posts.deleteMany({likes: 0});

// 8.
db.posts.find();

// Movie Database
// 2.
db.movies.insertMany([
		{
			title: "The Godfather",
			year: 1972,
			director: "Francis Ford Coppola",
			genre: ["Crime", "Drama"]
		},
		{
			title: "Titanic",
			year: 1997,
			director: "James Cameron",
			genre: ["Drama", "Romance"]
		},
		{
			title: "The Lion King",
			year: 1994,
			director: "Roger Allers",
			genre: ["Animation", "Adventure", "Drama"]
		},
		{
			title: "Raiders of the Lost Ark",
			year: 1981,
			director: "Steven Spielberg",
			genre: ["Action", "Adventure"]
		},
		{
			title: "The Dark Knight",
			year: 2008,
			director: "Christopher Nolan",
			genre: ["Action", "Crime", "Drama"]
		}
	]);

// 3.
db.movies.find();

// 4.
db.movies.find({},{
	title: 1,
	year: 1
});

// 5.
db.movies.find({genre: "Crime"},{
	title: 1,
	director: 1
});

// 6.
db.movies.updateOne({title: "The Godfather"},
	{
		$set: {
		year: 1971
	}});

// 7.
db.movies.find({
	$and: [
	{genre: "Drama"},
{
	year: {
		$lt: 1990
	}
}]});